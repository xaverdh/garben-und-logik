# Garben und Logik

## Worum geht es?

**Was genau liefert ein Beweis mehr als nur die Information, dass die bewiesene
Aussage stimmt? Was hat man davon, wenn man auf das Axiom, jede Aussage sei
wahr oder falsch, verzichtet?**

In dieser Vorlesung möchten wir mathematische Beweise als eigenständige
mathematische Objekte behandeln und von einem höheren Standpunkt aus
betrachten. Das ist mit Gentzens Kalkül natürlichen Schließens möglich,
welches erlaubt, informale Argumente zu formalisieren. Damit ausgerüstet können
wir präzise untersuchen, auf welchen logischen Prinzipien gegebene Beweise
beruhen.

Wir lernen intuitionistische Logik kennen, bei der das Prinzip vom
ausgeschlossenen Dritten ("jede Aussage stimmt oder stimmt nicht") nicht
verwendent wird, und verstehen Gödels Unvollständigkeitssatz, welcher in erster
Näherung besagt, dass es wahre Aussagen gibt, die nicht beweisbar sind.

Anschließend behandeln wir Realisierbarkeitstheorie. Damit können wir eine
Verbindung zwischen Logik und Berechenbarkeitstheorie aus der theoretischen
Informatik herstellen und einsehen, in welchem Sinn gewisse Prinzipien gelten,
die in klassischer Mathematik schlichtweg falsch sind: etwa die Behauptung,
dass jede Funktion ℝ → ℝ stetig sei. Das werden wir nutzen, um *proof mining*
zu betreiben: aus gegebenen Beweisen weitere Informationen wie obere Schranken
zu extrahieren. Dies werden wir mit Anwendungen in der Analysis und Numerik
beleuchten.

Schließlich erkunden wir die interne Sprache von Topoi, mathematischen
Alternativuniversen, in denen nicht die üblichen Gesetze der Logik gelten. Wir
werden sehen, wie man diese in Geometrie und Algebra gewinnbringend einsetzen
kann. Auch der effektive Topos, der ein weiteres Bindeglied zur theoretischen
Informatik darstellt und Realisierbarkeitstheorie gewissermaßen
vergegenständlicht, wird thematisiert werden.


## Termine

**Vorlesung:** Mittwochs 14:00 Uhr und freitags 8:15 Uhr in 2004/L1

**Übung:** Montag 17:30 Uhr in 1008/L1 (mit optionaler anschließender Brotzeit)


## Übungsblätter

Die Übungsblätter entstehen nach und nach auf
https://iblech.gitlab.io/garben-und-logik/uebungen.pdf. Anregungen sind immer
herzlich willkommen.


## Vorlesungsplan

In eckigen Klammern: mögliche Übungsaufgaben

1. **Grundlagen**
    * Woche 1: Vorlesungsüberblick und Gentzens Kalkül natürlichen Schließens
      *[Formalisierung von Beweisen, Brouwersche Gegenbeispiele]*
    * Woche 2: Übersetzungen und Gentzens Konsistenzbeweis von Peano-Arithmetik
      *[A-Übersetzung, Ordinalanalysis]*
    * Woche 3: Entscheidungsmethoden für gewisse formale Systeme
    * Woche 4: Gödels Vollständigkeitssatz und der Löwenheim–Skolemsche Satz
      *[Phänomene von Joel David Hamkins]*
2. **Arithmetisierung von Syntax**
    * Woche 5: Rekursive Funktionen und ihre Darstellung
    * Woche 6: Das Diagonallemma, Gödels Unvollständigkeitssatz und Konsequenzen
      *[Formalisierbarkeit von Wahrheit, Löbs Theorem, …]*
3. **Realisierbarkeitstheorie**
    * Woche 7: Einführung in die Realisierbarkeitstheorie
    * Woche 8: Konstruktive Prinzipien
      *[Spaß und Nutzen von Traumaxiomen, effektive Analysis nach Bishop/Bridges]*
    * Woche 9: *Puffer oder weitere Themen, etwa Unterschiede bei höheren Typen*
4. **Proof mining und rechnerischer Inhalt:** Was genau liefert ein Beweis mehr
   als nur die Information, dass die bewiesene Aussage stimmt?
    * Woche 10: Proof mining mittels der Realisierbarkeitsinterpretation
      *[Beweis von Metatheoremen und explizite Schrankenextraktion]*
    * Woche 11: Dynamische Methoden in der Algebra
      *[Extraktion von Zeugen]*
5. **Die interne Sprache von Topoi**
    * Woche 12: Grundlagen
    * Woche 13: Anwendungen in Algebra und Geometrie
    * Woche 14: Vollständigkeit von Garbensemantik
    * Woche 15: Der effektive Topos

Gewünscht, in diesen Plan aber noch nicht eingearbeitet:

* Kategorielle Modelle von Typentheorie
* Forcing (etwa um Unentscheidbarkeit der Kontinuumshypothese oder, vielleicht
  spannender, Unabhängigkeit des Auswahlaxioms zu zeigen)
* Diophantische Gleichungen (nur ganz kurzer Exkurs)

Kandidaten für Themen, die schneller (oder in Übungen) behandelt werden
könnten, um Zeit für diese zu schaffen:

* Entscheidungsmethoden für gewisse formale Systeme
* Rekursive Funktionen und ihre Darstellung
* Vollständigkeit von Garbensemantik

In den Übungen werden wir immer mal wieder mit Theoremumgebungen wie Coq, Agda,
Isabelle oder Nuprl arbeiten.


## Lernziele

Wir können präzisieren, auf welchen logischen Prinzipien gegebene
mathematische Argumentationen beruhen, und können Beweise formalisieren. Wir
verstehen die Beziehungen zwischen Syntax und Semantik und haben ein
reflektiertes Verständnis von Gödels Unvollständigkeitssätzen.

Weiterhin verstehen wir den Bezug von formaler Logik zur
Berechenbarkeitstheorie in der theoretischen Informatik. Wir kennen klassische
und nichtklassische Prinzipien, die in Realisierbarkeitsinterpretationen
intuitionistischer Logik gelten. Wir können in einfachen Fällen aus Beweisen
rechnerischen Inhalt wie obere Schranken extrahieren.

Wir verstehen die Grundlagen der internen Sprache von Topoi, kennen Anwendungen
in Algebra und Geometrie und können die Sprache in einfachen Situationen
selbstständig anwenden.


## Sheafification Man

![](aushang/sheafification-man.png)
