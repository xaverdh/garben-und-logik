=== Intuitionistische Logik

* LEM
* Schlüssel und Kate Moss
* BHK-Interpretation (informal)
* LPO_N
* Gesetze von de Morgan
* DNE <==> LEM


=== Gentzens Kalkül natürlichen Schließens

* Freie Variablen
* Kontexte
* Sequenzen
* Ableitungen


=== Übersetzungen

* Doppelnegationsübersetzung
* A-Übersetzung
* Äquikonsistenz von HA und PA


=== Konsistenz von PA

* Gentzens Beweis
* Ordinalanalysis
